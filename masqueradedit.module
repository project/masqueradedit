<?php

/**
 * @file
 * A module to allow editing of a node as its author with the Masquerade module.
 */

/**
 * Implements hook_menu().
 */
function masqueradedit_menu() {
  // Masquerade and edit button.
  $items['node/%node/masqueradedit'] = array(
    'title' => 'Edit as user',
    'title callback' => 'masqueradedit_title_callback',
    'title arguments' => array(1),
    'page callback' => 'masqueradedit_page',
    'page arguments' => array(1),
    'access callback' => 'masqueradedit_access',
    'access arguments' => array(1),
    'weight' => -1,
    'type' => MENU_LOCAL_TASK,
  );

  return $items;
}

/**
 * Title callback for masqueradedit tab.
 *
 * @param object $node
 *   The node to be edited.
 *
 * @return string
 *   A title string with the username, or "edit as user" on fail.
 */
function masqueradedit_title_callback($node) {
  $uid = $node->uid;
  $user = ($uid) ? user_load($uid) : NULL;
  $name = ($user) ? $user->name : 'user';

  $title = t('Edit as ') . t($name);

  return filter_xss($title);
}

/**
 * Custom access callback for masqueradedit.
 *
 * @param object $node
 *   A node object passed by the menu callback.
 *
 * @return bool
 *   An access return.
 */
function masqueradedit_access($node) {
  global $user;

  $access = FALSE;

  if (!isset($_SESSION['masquerading']) && $user->uid != $node->uid) {
    $access = user_access('masquerade as user');
  }

  // See if any other modules are using our hook.
  drupal_alter('masqueradedit_access', $node, $access);

  if (is_bool($access)) {
    return $access;
  }
  else {
    return FALSE;
  }
}

/**
 * Custom page callback to masquerade as a node author whilst editing.
 *
 * @param object $node
 *   A node object passed by the menu callback.
 */
function masqueradedit_page($node) {

  // Build our variables.
  $uid = $node->uid;
  $switch_link = 'masquerade/switch/' . $uid;
  $destination = 'node/' . $node->nid . '/edit';

  // Allow hooking of the destination.
  drupal_alter('masqueradedit_destination', $node, $destination);

  $_SESSION['masqueradediting'] = TRUE;

  // Redirect user through the masquerade URL to the edit page.
  drupal_goto($switch_link, array(
    'query' => array(
      'token' => drupal_get_token($switch_link),
      'destination' => $destination,
    ),
  ));
}

/**
 * Implements hook_node_update().
 */
function masqueradedit_node_update($node) {
  if (isset($_SESSION['masquerading'])) {

    // If a user is masquerading, we have to update the uid.
    $actual_uid = (int) $_SESSION['masquerading'];

    $revision_uid_updated = db_update('node_revision')
      ->fields(array('uid' => $actual_uid))
      ->condition('vid', $node->vid, '=')
      ->execute();

    if (!$revision_uid_updated) {
      watchdog('Masqueradedit', 'The revision for %node could not be amended. Please check the node for any errors', array('%node' => $node->nid), WATCHDOG_NOTICE);
    }
  }
}

/**
 * Implements hook_form_alter().
 */
function masqueradedit_form_alter(&$form, &$form_state, $form_id) {
  if (array_key_exists('#node_edit_form', $form) && isset($_SESSION['masqueradediting'])) {
    if (array_key_exists('submit', $form['actions'])) {
      $form['actions']['masqueradedit'] = $form['actions']['submit'];
      $form['actions']['masqueradedit']['#access'] = TRUE;
      $form['actions']['masqueradedit']['#value'] = t('Save and switch back');
      $form['actions']['masqueradedit']['#submit'][] = 'masqueradedit_back';
    }
    else {
      $form['actions']['masqueradedit'] = array(
        '#type' => 'submit',
        '#value' => t('Save and switch back'),
        '#submit' => array('node_form_submit', 'masqueradedit_back'),
      );
    }
  }
}

/**
 * Implements hook_node_presave().
 */
function masqueradedit_node_presave($node) {
  if (isset($_SESSION['masquerading'])) {

    // If a user is masquerading, log this.
    $actual_uid = (int) $_SESSION['masquerading'];
    $actual_user = user_load($actual_uid);

    if ($node->name) {
      $node->log = 'Edited by ' . $actual_user->name . ' masquerading as ' . $node->name;
    }
  }
}

/**
 * Custom form submit to switch back from masquerading.
 */
function masqueradedit_back(&$form, &$form_state) {
  // Build our variables.
  $switch_link = 'masquerade/unswitch';
  $destination = 'node/' . $form['nid']['#value'];

  // Allow hooking of the destination.
  drupal_alter('masqueradedit_back_destination', $form, $destination);

  unset($_SESSION['masqueradediting']);

  // Redirect user through the masquerade URL to the edit page.
  drupal_goto($switch_link, array(
    'query' => array(
      'token' => drupal_get_token($switch_link),
      'destination' => $destination),
  ));
}

/**
 * Implements hook_entity_view().
 */
function masqueradedit_entity_view($entity, $type, $view_mode, $langcode) {

  // If user has left masqueradedit without using the cancel ...
  if (isset($_SESSION['masqueradediting'])) {

    global $theme;

    $block = block_load('masquerade', 'masquerade');

    if ($block) {

      // Check if the masquerade block is enabled.
      if ($block->theme != $theme && $block->staus != 1) {

        // And check if they are still masquerading.
        if (isset($_SESSION['masquerading'])) {

          // And unswitch them.
          $nid = NULL;

          switch ($type) {
            case 'node':
              $nid = $entity->nid;
              break;

            case 'field_collection_item':
              $host = $entity->hostEntity();
              $nid = $host->nid;
              break;
          }

          $form = ($nid) ? array('nid' => array('#value' => $nid), '#fake_form' => TRUE) : array();

          $form_state = array();

          masqueradedit_back($form, $form_state);
        }
      }
      else {

        // Otherwise, let them choose and reset the $_Session.
        unset($_SESSION['masqueradediting']);
      }
    }
  }
}
